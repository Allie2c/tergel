import React from 'react';
import { Link } from 'react-router-dom';

function Test() {
  return (
    <div>
      <h1>Test Page</h1>
      <Link to="/">
        <button>Go to Home Page</button>
      </Link>
    </div>
  );
}

export default Test;
